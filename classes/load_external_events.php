<?php
class load_external_events{
	protected $event = "";
	public $mydb = '';
	public $url = '';

	public function __construct()
	{
		global $wpdb, $datenbank;
		$this->url = meineGaesteliste::url();
		$db = $datenbank[0];
		$mydb = new DB_MySQL($db['localhost'], $db['db'], $db['username'], $db['password']);
		$this->mydb = $mydb;

		$this->display();
	}
	public function display(){
	?>
        <div id="dashboard-widgets" class="metabox-holder">
        	 <div id="postbox-container-1" class="postbox-container">
             	<div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">External Events</h2>
                        <div class=""><?php echo $this->load_extern_events() ?></div>
                    </div>
                </div>
             </div>
             <div id="postbox-container-2" class="postbox-container">
             	<div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">Newsletter Events</h2>
                        <div class=""><?php echo $this->before_delete_newsletter() ?></div>
                    </div>
                </div>
             </div>
		</div>
		<?php
	}
	public function sql_event(){
		global $wpdb, $ms_prefix;
		$type = isset($_GET['type']) ? $_GET['type'] : NULL;
		$mydb = $this->mydb;
		$url  = $this->url;

		$sql = "SELECT * FROM ".$ms_prefix."em_events WHERE recurrence=0 AND event_start_date >= '".current_time("Y-m-d")."' ORDER BY event_start_date ASC";
    return $mydb->query($sql);

	}
	public function load_extern_events(){
		global $wpdb, $ms_prefix;

		$type = isset($_GET['type']) ? $_GET['type'] : NULL;
		$mydb = $this->mydb;
		$url  = $this->url;

		$out = '<table class="wp-list-table widefat striped">';
		$out .= '<tr><th colspan="4"><strong>Externe Events werden in die Newsletter-Events gespeichert.</strong></th></tr>';
		$events = $this->sql_event();

    if ( count( $events ) < 1 ){
      return '';
    }
		// $events =  $mydb->fetch_all($eventsQuery);
    foreach( $events as $event ) {
      // var_dump( $event );
			 $out .= '<tr>';
			 $out .= '<td>'.date_i18n('l,d.m.Y', strtotime($event->event_start_date)).'</td>';
			 $out .= '<td>'.$event->event_id.'</td>';
			 $out .= '<td>'.$event->event_name.'</td>';
			 $out .= '<td>' . $this->save_extern_events_to_newsletter_events( $event ) .'</td>';
			 $out .= '</tr>';
		  }
		  $out .= '</table>';

		  return $out;
	}
	public function save_extern_events_to_newsletter_events( $event ){
		global $wpdb;
		$event_id = $event->event_id;
		$event_datum = $event->event_start_date;

		if (!$event_id){
			return '';
		}

		$sql = 'select * from wiml_maillist_newsletter WHERE event_datum >= "'.current_time('Y-m-d').'" AND event_id="'.$event_id.'"';
		$newsletter = $wpdb->get_row($sql, ARRAY_A);
      if ( !is_object($newsletter) ){
  			$wpdb->insert(
  				'wiml_maillist_newsletter',
  				array(
  					'event_id'		=> $event_id,
  					'post_id'		  => $event->post_id,
  					'event_datum'	=> $event_datum,
  					'name'			  => $event->event_name,
  					'description'	=> $event->post_content
  				),
  				array( '%d', '%d', '%s','%s', '%s' )
  			);
  			return $wpdb->insert_id .'<span class="wp-ui-text-notification">NEU hinzugefügt</span>';
  		} else {
  			return 'Newsletter exists: ' . $newsletter->id;
  		}
	}
	public function before_delete_newsletter(){
		global $wpdb;
		$sql = 'select * from wiml_maillist_newsletter WHERE (template_name="" OR template_name IS NULL ) AND event_datum >= "'.current_time('Y-m-d').'" GROUP BY event_datum ORDER BY event_datum asc';
		$newsletters = $wpdb->get_results( $sql );

		$out = '<table class="wp-list-table widefat striped">';
		$out .= '<tr><th colspan="3"><strong>Doppelte Newseltter-Events werden gelöscht. Die Event_id von Newseltter-Events wird mit der Event_id aus den Externen Events verglichen.</strong></th></tr>';
		//var_dump( $newsletters );
		foreach ( $newsletters as $newsletter )
		{
			$out .= '<tr>';
			$out .= '<td>' . date_i18n('l,d.m.Y', strtotime($newsletter->event_datum)) . '</td>';
			$out .= '<td>' . $newsletter->name.'</td>';
			$out .= '<td>' . $this->check_exists_extern_event( $newsletter->event_id ) . '</td>';
			$out .= '</tr>';
		}
		$out .= '</table>';
		return $out;
	}
	public function check_exists_extern_event( $event_id ){
		global $wpdb, $ms_prefix;
		$mydb     = $this->mydb;
    $event_id = isset($event_id) ? $event_id : NULL;

		$sql = "SELECT * FROM ".$ms_prefix."em_events WHERE recurrence=0 AND event_start_date >= '".current_time("Y-m-d")."' AND event_id='".$event_id."' LIMIT 1";
    $event  = $mydb->fetch_row($sql);

		if( !$event->event_id ){
			$wpdb->delete( 'wiml_maillist_newsletter', array( 'event_id' => $event_id ) );
			return 'DELETE';
		}
    $sql = 'select * from wiml_maillist_newsletter WHERE event_datum >= "'.current_time("Y-m-d").'" AND event_id="'.$event_id.'"';
    $newsletters = $wpdb->get_results( $sql );
    if(count($newsletters) > 1){
      $wpdb->delete( 'wiml_maillist_newsletter', array( 'event_id' => $event_id ) );
      return 'DELETE';
    }
		return 'ID-Extern-Event: ' . $event_id . ' - Anz: ' . count($newsletters);
	}
}
?>
