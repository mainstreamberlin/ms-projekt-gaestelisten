<?php
class event_list_table extends WP_List_Table {
	/** Class constructor */
	public function __construct() {

		parent::__construct( array(
			'singular' => 'Gaesteliste', //singular name of the listed records
			'plural'   => 'Gaestelisten', //plural name of the listed records
			'ajax'     => true //should this table support ajax?
		));
	}
	public function table_data( $per_page = 10, $page_number = 1 )
    {
		global $wpdb;
        $data = array();
		$sql = 'select id, event_id, name as title, event_datum, (select count(nid) from wiml_gaesteliste where wiml_gaesteliste.nid=wiml_maillist_newsletter.id group by nid) as anzahl
				from wiml_maillist_newsletter ';
		if ( ! empty( $_REQUEST['eventtime'] ) && $_REQUEST['eventtime'] == 'last') {
			$sql .= ' WHERE event_datum <= "'.current_time('Y-m-d').'"';
		} else {
			$sql .= ' WHERE event_datum >= "'.current_time('Y-m-d').'"';
		}
		if ( ! empty( $_REQUEST['orderby'] ) ) {

			$sql .= ' ORDER BY ' . esc_sql( $_REQUEST['orderby'] );
			$sql .= ! empty( $_REQUEST['order'] ) ? ' ' . esc_sql( $_REQUEST['order'] ) : ' ASC';

		} else {
			if ( ! empty( $_REQUEST['eventtime'] ) && $_REQUEST['eventtime'] == 'last') {
			$sql .= ' ORDER BY event_datum desc ';
			} else {
			$sql .= ' ORDER BY event_datum asc ';
			}
		}

		$sql .= " LIMIT ".$per_page;
		$sql .= ' OFFSET ' . ( $page_number - 1 ) * $per_page;

    $events = $wpdb->get_results($sql ,ARRAY_A);
    if ( count($events) > 0 ){
      return $events;
    }
		return;
  }
	public static function record_count() {
		global $wpdb;
		if ( ! empty( $_REQUEST['eventtime'] ) && $_REQUEST['eventtime'] == 'last') {
		$sql = 'SELECT COUNT(*) FROM wiml_maillist_newsletter WHERE event_datum <= "'.current_time('Y-m-d').'"';
		} else {
		$sql = 'SELECT COUNT(*) FROM wiml_maillist_newsletter WHERE event_datum >= "'.current_time('Y-m-d').'"';
		}
		return $wpdb->get_var( $sql );
	}

	function column_title($item){
		$event = (object) array();
		$event->event_id = $item["event_id"];
		$event->event_datum = $item["event_datum"];
		$actions = array(
			'upload'      => sprintf('<a href="?page=%s&view=%s&newsletterID=%s">Gästeliste hochladen</a>',$_REQUEST['page'],'upload',$item['id']),
			'show'      => sprintf('<a target="_blank" href="'. meineGaesteliste::url_admin_ajax_show_gaesteliste($event, "&newsletterID=" . $item['id']) .'">Gästeliste drucken</a>'),
		);
		return sprintf('%1$s %2$s',$item['title'],$this->row_actions($actions));
	}


	public function column_default( $item, $column_name )
    {
        switch( $column_name ) {

            case 'title':
            case 'anzahl':
                return $item[ $column_name ];
			case 'event_datum':
				return date_i18n( "l, d.m.Y", strtotime($item[ $column_name ]) );
			default:
                return print_r( $item, true ) ;
        }
    }

	function get_columns() {
	   $columns = array(
      'title'      	=> 'Title',
      'event_datum' 	=> 'Datum',
      'anzahl'        => 'Anzahl',
    );
	  return $columns;
	}

	public function get_sortable_columns() {
	  $sortable_columns = array(
		'title' => array( 'title', true ),
		'event_datum' => array( 'event_datum', true )
	  );

	  return $sortable_columns;
	}
  public function get_hidden_columns(){
      return array();
  }

	public function prepare_items() {

    $columns                = $this->get_columns();
    $hidden                 = $this->get_hidden_columns();
    $sortable               = $this->get_sortable_columns();
		$this->_column_headers  = array($columns, $hidden, $sortable);

		/** Process bulk action */
		$this->process_bulk_action();

		$user			= get_current_user_id();
		$screen			= get_current_screen();
		$screen_option	= $screen->get_option('per_page', 'option');
		$per_page		= get_user_meta($user, $screen_option, true);
		$current_page	= $this->get_pagenum();
		$total_items 	= self::record_count();

		if ( empty ( $per_page ) || $per_page < 1 ) {
			$per_page	= $screen->get_option( 'per_page', 'default' );
		}

		$this->set_pagination_args( array(
		  'total_items' => $total_items, //WE have to calculate the total number of items
		  'per_page'    => $per_page //WE have to determine how many items to show on a page
		) );


		$this->items = self::table_data( $per_page, $current_page );
	}
	function extra_tablenav($which) {
		if($which == 'top'){
		$actions  = array(  'next' => 'kommende Events', 'last' => 'Vergangen Events', );
		$selected = !empty( $_REQUEST['eventtime'] ) ? $_REQUEST['eventtime'] : '';
		?>
		<div class="alignleft actions">
			<select name="eventtime">
				<?php foreach ( $actions as $k => $v ) : ?>
					<option value="<?php echo esc_attr( $k ); ?>" <?php echo ($k == $selected) ? 'selected="selected"' : '' ?>><?php echo esc_html( $v ); ?></option>
				<?php endforeach; ?>
			</select>

			<?php submit_button( __( 'Filter'), 'secondary', false, false, array( 'id' => 'post-query-submit' ) ); ?>

		</div>
		<?php
		}
	}
	function process_bulk_action() {

		//Detect when a bulk action is being triggered...
		if( 'delete'===$this->current_action() ) {
			wp_die('Items deleted (or they would be if we had items to delete)!');
		}

	}
}
?>
