<?php
class msprint {
	var $db = '';
	var $data = '';
	var $event_id = '';

	function __construct($event_id, $data)
	{
		$this->event_id = $event_id;
		$this->data = $data;
	}

	function regData()
	{
		if (count($this->rootMeta())){
			$metas =  $this->rootMeta();
			foreach ($metas["booking_meta"] as $mKey => $meta)
			{
				if($meta)
				{
					foreach ($meta as $rKey => $registration)
					{
						foreach ($registration as $key => $value)
						{
							if ($key == 'first_name' || $key == 'last_name') $regData[$mKey][$key] = ucfirst($value);
						}
					}

				}
				if ( isset($_GET["sort"]) && $_GET["sort"]) $regData[$mKey]["booking_date"] = $metas["booking_date"][$mKey];
			}
		}
		return ($regData);
	}
	function rootMeta()
	{
		if (count($this->itemData())){
			foreach($this->itemData() as $items)
			{
				foreach($items as $key => $item)
				{
					//if($key == 'booking_meta') $rootMeta[] = $item;
					$rootMeta[ $key ][] = $item;
				}
			}
		}
		return $rootMeta;
	}

	function itemData()
	{
		$data = $this->rootdata();
		if(count($data)){
			foreach( $data as $item)
			{
				$itemData[] = $item;
			}
		}
		return $itemData;
	}
	function itemDataSingle()
	{
		$data = $this->rootdata();
		if(count($data)){
			foreach( $data as $items)
			{
				foreach($items as $key => $item)
				{
					$itemData[$items["booking_id"]][$key] = $item;
				}
			}
		}
		return $itemData;
	}
	function rootData()
	{
		return $this->data;

	}
	public function array_unique_deep($array) {
        foreach ($array as $k=>$na)
            $new[$k] = serialize($na);
        $uniq = array_unique($new);
        foreach($uniq as $k=>$ser)
            $new1[$k] = unserialize($ser);
        return ($new1);
	}
	public function array_multi_unique($multiArray){
		/* array_unique() für multidimensionale Arrays
		 * @param    array    $multiArray = array(array(..), array(..), ..)
		 * @return   array    Array mit einmaligen Elementen
		**/
		$uniqueArray = array();
		// alle Array-Elemente durchgehen
		foreach($multiArray as $subArray){

		  // prüfen, ob Element bereits im Unique-Array
		  if(!in_array($subArray, $uniqueArray)){
			// Element hinzufügen, wenn noch nicht drin
			$uniqueArray[] = $subArray;
		  }
		}
		return $uniqueArray;
	}
}
?>
