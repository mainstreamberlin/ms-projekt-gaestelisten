<?php
class gaesteliste_insert_names
{
	public $event = '';

	public function __construct(  $event  ){
		$this->event =  $event;
		$this->save();
	}
	public function save(){
		global $wpdb;
		if( isset($_REQUEST["save_name_to_gaesteliste"]) && isset($_REQUEST["firstname"]) ){
			$sql = 'INSERT wiml_gaesteliste SET
						firstname="'.$_REQUEST["firstname"].'",
						lastname="'.$_REQUEST["lastname"].'",
						plus="'.$_REQUEST["plus"].'",
						aktion="'.$_REQUEST["aktion"].'",
						importdatum="'. $this->event->event_datum.'",
						importdatei="private",
						nid="'.$_GET["newsletterID"].'"';
			$res = $wpdb->query($sql);
			if( $res ){
			?><div class="updated notice"><p>Gespeichert: <?php echo $_REQUEST["firstname"] ?> | <?php echo $_REQUEST["lastname"] ?> | <?php echo $_REQUEST["plus"] ?> | <?php echo $_REQUEST["aktion"] ?><p></div><?php
			$_POST = "";
			} else {
			?><div class="error notice"> <p>ERROR .... Überprüfen bitte die Daten!</p></div><?php
			}
		}
	}
	public function display(){
		$inputs = array(
			array("name" => "firstname", "title" => "Vorname"),
			array("name" => "lastname", "title" => "Nachname"),
			array("name" => "plus", "title" => "Plus"),
			array("name" => "aktion", "title" => "Aktion")
		);
		?>
        <form method="post" action="<?php echo meineGaesteliste::url() ?>">
        <table class="input-text-wrap" width="100%">
        	<?php foreach( $inputs as $input ) : ?>
            <tr>
            	<td><label for="<?php echo $input["name"] ?>" class=""><?php echo $input["title"] ?></label></td><td><input name="<?php echo $input["name"] ?>" id="<?php echo $input["name"] ?>" value="<?php echo isset($_POST[$input["name"]]) ? $_POST[$input["name"]] : '' ?>"></td>
            </tr>
            <?php endforeach; ?>
            <tr>
            	<td></td><td><input type="submit" name="save_name_to_gaesteliste" value="Speichern" id="save_name_to_gaesteliste" class="button button-primary"></td>
            </tr>
        </table>
        </form>
        <?php
	}
}
?>
