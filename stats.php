<?php
/**
 *
 */
class gaestelistenStats
{

  function __construct()
  {
    # code...
  }
  public function display()
  {
    $args = array(
    "postbox_class" => array("first" => "", "second" => "", "third" => "")
    );

    $widgets = array(
    array("align" => "first", "title" => "Webseiten", "data" => $this->eventsWeb() ),

    array("align" => "second",  "height" => 100,  "title" => "Anmeldungen pro Monat im Jahr " . current_time("Y"), "data" => $this->anmeldungenPerMonth() ),
    array("align" => "third",   "height" => 100,  "title" => "Anmeldungen pro Jahr", "data" => $this->anmeldungenPerYear() ),
    );

    $MSDashboard = new MSDashboard( $widgets, $args );
    echo $MSDashboard->display();
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function anmeldungenPerMonth()
  {
    global $wpdb;
    $sql = "SELECT COUNT(*) as anz, DATE_FORMAT(booking_datum, '%M') as month FROM wiml_users_events WHERE (!email_adress OR email_adress!='') AND YEAR(booking_datum)='".current_time("Y")."' GROUP BY MONTH(booking_datum) ORDER BY MONTH(booking_datum) desc";
    $users = $wpdb->get_results( $sql );
    $gesamt = '';
    $args = array(
      "maxHeight" => 100,
      "maxWidth"  => 550,
      "unit"  => 10,
      "barWidth"  => 60,
      "x" => array("title" => "Monat", "col" => "month"),
      "y" => array("title" => "Anzahl", "col" => "anz"),
      "data" => $users);

    $gesamt   = 0;
    $MSCharts = new MSCharts( $args );
    $out      = $MSCharts->bar();

    $out .= '<table class="wp-list-table widefat striped">';
    foreach( $users as $user ) :

        $out .= '<tr>';
        $out .= '<td>'. date_i18n("F", strtotime(current_time("Y").'-'.$user->month)) .'</td>';
        $out .= '<td>'. $user->anz .'</td>';
        $out .= '</tr>';

        $gesamt += $user->anz;
    endforeach;
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td></tr>';
    $out .= '</table>';
    return $out;
  }
  /**
 	 * Block comment
 	 *
 	 * @param type
 	 * @return void
	 */
	public function anmeldungenPerYear()
  {
    global $wpdb;
    $sql      = "SELECT COUNT(*) as anz, YEAR(booking_datum) as year FROM wiml_users_events WHERE (!email_adress OR email_adress!='') GROUP BY YEAR(booking_datum) ORDER BY YEAR(booking_datum) desc";
    $users    = $wpdb->get_results( $sql );
    $gesamt   = 0;
    $args     = array(
      "maxHeight" => 100,
      "maxWidth"  => 550,
      "unit"  => 100,
      "barWidth"  => 40,
      "x" => array("title" => "Monat", "col" => "year"),
      "y" => array("title" => "Anzahl", "col" => "anz"),
      "data" => $users);

    $MSCharts = new MSCharts( $args );
    $out = $MSCharts->bar();

    $out .= '<table class="wp-list-table widefat striped">';
    foreach( $users as $user ) :

        $out .= '<tr>';
        $out .= '<td>'. $user->year .'</td>';
        $out .= '<td>'. $user->anz .'</td>';
        $out .= '</tr>';

        $gesamt += $user->anz;
    endforeach;
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td></tr>';
    $out .= '</table>';
    return $out;
  }
  /**
  * Block comment
  *
  * @param type
  * @return void
  */
  public function eventsWeb()
  {
    global $wpdb;
    $gesamt = 0;
    $newUsers = array();
    $sql = "SELECT COUNT(*) as anz, booking_web FROM wiml_users_events WHERE (!booking_web OR booking_web!='') GROUP BY booking_web ORDER BY anz desc";
    /*var_dump($sql);*/
    $users = $wpdb->get_results( $sql );
    $out = '<table class="wp-list-table widefat striped">';
    foreach( $users as $user ){
      if( $user->booking_web ){
        $newUsers[$this->get_domain($user->booking_web)][] = $user;
      }
    }
    foreach( $newUsers as $userKey => $user ) :
        $anz = 0;
        foreach( $user as $u ) $anz += $u->anz;

        $out .= '<tr>';
        $out .= '<td>'. $userKey .'</td>';
        $out .= '<td>'. $anz .'</td>';
        $out .= '</tr>';

        $gesamt += $anz;
    endforeach;
    $out .= '<tr><td>Gesamt</td><td>'.$gesamt.'</td></tr>';
    $out .= '</table>';

    return $out;
  }
  public function get_domain($url)
  {
      $urlobj=parse_url($url);
      $domain=$urlobj['host'];
      if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $regs['domain'];
      }
      return false;
  }
}

?>
