
jQuery.urlParam = function(name){
	var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
	if (results==null){
       return null;
    }
    else{
       return results[1] || 0;
    }
}

if(jQuery.urlParam('save_registered_list') == "true") {
	
	setInterval(function(){ 
		if( jQuery(".guestlist li:last-child .result").text() == 1 ){
			setTimeout( function(){ 
				window.location.href = jQuery("#save_registered_list").data("redirect"); 
			},500 );
		}
	}, 3000);	
}

if(jQuery.urlParam('save_uploaded_file') == "speichern") {
	
	setInterval(function(){ 
		if( jQuery("#postbox-3-3 table tr:last-child .result").text() == 1 ){
			setTimeout( function(){ 
				window.location.href = jQuery("#save_uploaded_file").data("redirect"); 
			},500 );
		}
		
	}, 3000);	
}
