<?php
class view_upload{
	protected $event = "";
	public $mydb = '';
	public $url = '';
	public function __construct()
	{
		global $wpdb, $datenbank;
		if(!$_GET["newsletterID"]){
			wp_die();
		}
		$this->url = meineGaesteliste::url();
		$db = $datenbank[0];
		$mydb = new DB_MySQL($db['localhost'], $db['db'], $db['username'], $db['password']);
		$this->mydb = $mydb;

		$events_sql = "select * from wiml_maillist_newsletter WHERE id = '".$_GET["newsletterID"]."'";
		$event = $wpdb->get_row($events_sql);
		$current_gaestelisten = $wpdb->get_var("select count(id) as anz from wiml_gaesteliste where nid='". $_GET["newsletterID"] ."'");
		$email = urlencode($event->name );
		$this->event = $event;
		$gaesteliste_insert_names = new gaesteliste_insert_names(  $event  );
		$gaestelisten_upload = new gaestelisten_upload;
		?>
		<div class="metabox-holder">
            <div class="postbox gaestelisten-all-files">
            <a target="_blank" href="<?php echo meineGaesteliste::url_admin_ajax_show_gaesteliste($event, "&newsletterID=" .$_REQUEST["newsletterID"] ) ?>" class="handlediv button button-primary">Gästeliste drucken</a>
            <a href="<?php echo meineGaesteliste::remove_parameter("&view=upload") ?>&view=show" class="handlediv button">Gästeliste ansehen</a>
            <h2 class="hndle ui-sortable-handle"><span><?php echo date_i18n("l, d.m.Y", strtotime($event->event_datum) ) ?> - <?php echo $event->name ?> | Anzahl : <?php echo $current_gaestelisten ?></span></h2>
            <div class="inside"><?php $this->view_upload_gaestelisten_upload(); ?></div>
            </div>
        </div>
        <div id="dashboard-widgets" class="metabox-holder">

            <div id="postbox-container-1" class="postbox-container">
            	<div class="meta-box-sortables ui-sortable">
                	<div class="postbox">
                        <h2 class="hndle ui-sortable-handle">Name in die Gästeliste eintragen</h2>
                        <div class="inside"><?php $gaesteliste_insert_names->display() ?></div>
                    </div>
                    <div class="postbox">
                        <?php if( isset($_REQUEST["eventtime"]) && $_REQUEST["eventtime"] == "last"){ ?>
                        <a href="<?php echo meineGaesteliste::remove_parameter("&eventtime=last") ?>"  class="button handlediv">+ Aktuell</a>
                        <?php  } else { ?>
                        <a href="<?php echo $this->url ?>&eventtime=last"  class="button handlediv button-secondary">+ Last</a>
                        <?php } ?>
                        <h2 class="hndle ui-sortable-handle">MainStream Events</h2>
                        <div class="inside"><?php echo $this->load_extern_events()  ?></div>
                    </div>
            	</div>
            </div>
            <div id="postbox-container-2" class="postbox-container">
            	<div class="meta-box-sortables ui-sortable">
                    <div class="postbox">
                        <h2 class="hndle ui-sortable-handle">MainStream Gästeliste</h2>
                        <div class="inside"><?php echo $this->load_extern_events_gaesteliste()  ?></div>
                    </div>
                </div>
            </div>
            <div id="postbox-container-3" class="postbox-container last">
            	<div class="meta-box-sortables ui-sortable">
                    <div id="postbox-3-1" class="postbox">
                        <h2 class="hndle ui-sortable-handle">Upload Files</h2>
                        <div class="inside"><?php echo $this->upload_file_area()  ?></div>
                    </div>
                    <div id="postbox-3-2"><?php echo $this->show_all_uploaded_files() ?></div>
                    <div id="postbox-3-3" class="postbox"><?php echo $this->show_table_uploaded_file() ?></div>
                </div>
            </div>
        </div>
        <?php
	}
	public function view_upload_gaestelisten_upload(){
		global $wpdb;

		if( isset($_GET["delete"]) && $_GET["delete"] == TRUE){
					$output .= '<div class="error notice">';
					$output .= '<p>Willst du wirklich diese Datei (<i>'.$_GET['file'].' | '.$_GET['importdatum'].'</i>) löschen? <a class="button button-primary" href="'.$this->url.'&delete=true&delete_now=true&importdatum='.$_GET['importdatum'].'&file='.$_GET['file'].'">Löschen</a></p>' ;
					$output .= '</div>';
					if($_GET["delete_now"]){
						$sql1 = 'DELETE FROM wiml_gaesteliste where nid="'.$_GET["newsletterID"].'" and importdatum="'.$_GET['importdatum'].'"';
						$wpdb->query($sql1);
					}
					echo $output;
		}
		?>
		<table cellpadding="5" cellspacing="1" border="0" width="100%">
		<?php
		$current_gaestelisten = $wpdb->get_results("select *, count(importdatum) as anz from wiml_gaesteliste where nid='".$this->event->id."' group by importdatum");
		foreach ($current_gaestelisten as $cg)
		{
		?>
			<tr>
				<td><?php echo $cg->importdatum ?></td>
				<td><?php echo $cg->anz ?></td>
				<td><?php echo $cg->importdatei ?></td>
				<td align="center"><a class="button button-primary" href="<?php echo $this->url  ?>&delete=true&importdatum=<?php echo  $cg->importdatum ?>&file=<?php echo $cg->importdatei ?>">Löschen</a></td>
				<td align="right"><a class="button" href="<?php echo meineGaesteliste::remove_parameter("&view=upload") ?>&view=show&importdatum=<?php echo $cg->importdatum ?>">Ansehen</a></td>
                <td align="right"><a target="_blank" class="button" href="<?php echo meineGaesteliste::url_admin_ajax_show_gaesteliste($this->event, "&newsletterID=" .$_REQUEST["newsletterID"]. "&importdatum=" . $cg->importdatum) ?>">Drucken</a></td>
            </tr>
		<?php
		}
		?>
		</table>
	<?php
	}
	function load_extern_events(){
		  global $wpdb, $ms_prefix;

		  $type= isset($_GET['type']) ? $_GET['type'] : NULL;
		  $event_id = isset($_REQUEST["event_id"]) ? $_REQUEST["event_id"] : NULL;
		  $mydb = $this->mydb;
		  $url = $this->url;

		  $out = '<ul class="events">';
		  if( isset($_REQUEST["eventtime"]) && $_REQUEST["eventtime"] == "last"){
		  $sql = "SELECT * FROM ".$ms_prefix."em_events WHERE event_start_date <= '".current_time("Y-m-d")."' ORDER BY event_start_date DESC limit 30";
		  } else {
		  $sql = "SELECT * FROM ".$ms_prefix."em_events WHERE event_start_date >= '".current_time("Y-m-d")."' ORDER BY event_start_date ASC";
		  }
		  $events = $mydb->query($sql);
      if (count($events) < 1){
        return '';
      }
		  foreach( $events as $event ) {
			  $out .= '<li><a class="button" href="'. $url.'&event_id='.$event->event_id.'"><i class="dashicons dashicons-plus"></i></a><span>'.date_i18n('l,d.m.Y', strtotime($event->event_start_date)).' - '.$event->event_name.'</span></li>';
		  }
		  $out .= '</ul>';

		  return $out;
	}
	function load_extern_events_gaesteliste(){
		global $wpdb, $datenbank, $ms_prefix;

		$event_id = isset($_REQUEST["event_id"]) ? $_REQUEST["event_id"] : NULL;
		$mydb = $this->mydb;

		if(!$event_id){
			return '';
		}
		$sql = "select * from ".$ms_prefix."em_bookings WHERE booking_status=1 and event_id = '".$event_id."'";
		$rootData = $mydb->query( $sql );
    $data = array();
		foreach( $rootData as $r ) {
			$data[] = array(
				  'booking_id'	=>	$r->booking_id,
				  'event_id'	=>	$r->event_id,
				  'person_id'	=>	$r->person_id,
				  'booking_spaces' 	=>	$r->booking_spaces,
				  'booking_comment' =>	$r->booking_comment,
				  'booking_date' 	=>	$r->booking_date,
				  'booking_status' 	=>	$r->booking_status,
				  'booking_price' 	=>	$r->booking_price,
				  'booking_meta' 	=>	unserialize($r->booking_meta),
				  'booking_code'	=>	isset($r->booking_code) ? $r->booking_code : '',
			);
		}

		$gl = new msprint($event_id, $data);
		$dataGL = $gl->regData();
		if(count($dataGL)){
			$dataGL= $gl->array_unique_deep($dataGL);
			asort($dataGL);

			$out = '<form>';
			$out .= '<input type="hidden" name="page" value="'.$_GET['page'].'">';
			$out .= '<input type="hidden" name="view" value="'.$_GET['view'].'">';
			$out .= '<input type="hidden" name="newsletterID" value="'.$_GET['newsletterID'].'">';
			$out .= '<input type="hidden" name="event_id" value="'.$_GET['event_id'].'">';
			$out .= '<input type="hidden" name="eventtime" value="'.$_GET['eventtime'].'">';
			$out .= '<input type="hidden" name="save_registered_list" value="true">';

			$out .= "<TABLE BORDER=\"0\">\n";
			$out .= '<tr>';
			$out .= '<td><input type="submit" id="save_registered_list" name="save" value="Diese Liste speichern" class="button button-primary" data-redirect="'.$this->url.'"></td>';
			$out .=  "</TR>";
			$out .=  "</table>";
			$out .= '</form>';

			$out .= '<ul class="guestlist" data-redirect="'.$this->url.'">
					<li class="thead"><div class="firstname"><strong>Vorname</strong></div><div class="lastname"><strong>Nachname</strong></div><div class="anz"><strong>Anz: '.count($dataGL).'</strong></div></li>';

			$k=1;
			$time = time();
			$time = DATE("Y-m-d H:i:s" ,$time);
			foreach ($dataGL as  $g) {
				$h = '';

				$out .= '<li class="guestlist">';
				foreach ($g as $t => $n){

					if($t == 'first_name'){
						$out .= ($n) ? '<div class="firstname">'. $n .'</div>' : '';
						$update = 'firstname="'.$n.'"';
						$vorname = $n;
					}
					if($t == 'last_name'){
						$out .= ($n) ? '<div class="lastname">'. $n .'</div>' : '';
						$update .= ($update) ? ',' : '';
						$update .= ' lastname="'.$n.'"';
						$nachname = $n;
					}

				}
				$k++;
        $res = '';
        $p = '';
				if( isset($_GET["save_registered_list"]) && $_GET["save_registered_list"] == TRUE){
					$sql = 'INSERT wiml_gaesteliste SET '.$update .', importdatum="'.$time.'", importdatei="mainstream", nid="'.$_GET["newsletterID"].'"';
					$res = $wpdb->query($sql);
				}
				$out .= '<div class="result">'.$res.'</div>';
				$out .= '</li>';
				$p = $res;
				$update = '';
			}
			$out .= '</ul>';
		}
		return $out;
	}
	public function upload_file_area(){
	?>
	  <table width="100%">
		<tr>
			<td valign="top">
				<form action="<?php echo admin_url("admin.php") ?>?page=mainstream-guestlist&view=<?php echo $_REQUEST["view"] ?>&newsletterID=<?php echo $_REQUEST["newsletterID"] ?>" method="post" enctype="multipart/form-data">
				<p><label for="file">Filename:</label>
				<input type="file" name="file" id="file" />
				<input type="hidden" name="page" value="<?php echo $_REQUEST["page"] ?>" /> </p>
				<p><input type="submit" name="submit" value="Datei hochladen" class="button button-primary" /></p>
				</form>
			</td>
		</tr>
	  </table>
	<?php
    }
	public function show_all_uploaded_files(){
		$upload_show = new upload_show;
		$upload_show->prepare_items();
		return $upload_show->display();
	}
	public function show_table_uploaded_file(){
		$upload_datei_show = new upload_datei_show;
		return $upload_datei_show->show();
	}
}
?>
